"use strict";

const Queue = require('better-queue');

const axios = require('axios');

const EtagRequest = require(`./EtagRequest`);

class RequestQueue {
  constructor({
    concurrent = 30,
    maxTimeout = 10000,
    maxRetries = 3,
    retryDelay = 1000,
    cacheGet = null,
    cacheSet = null,
    reporter = null
  }) {
    this.push = async item => {
      const self = this;
      return new Promise((resolve, reject) => {
        self._queue.push(item).on('finish', result => {
          resolve(result);
        }).on('failed', err => {
          reject(err);
        });
      });
    };

    this.handleQueueItem = async (item, cb) => {
      const self = this;
      console.log(`fetching ${item.url}`);

      try {
        const filteredItem = ({
          auth,
          headers,
          params
        }) => ({
          auth,
          headers,
          params
        });

        const request = await axios.get(item.url, { ...filteredItem(item),
          timeout: self._settings.maxTimeout - 100
        }); // item._resolver(request)

        cb(null, request);
      } catch (error) {
        console.error(`${item.url} failed`); // debugger

        if (error !== null && error !== void 0 && error.isAxiosError) {
          var _error$response;

          if (error.code === 'ECONNABORTED') {
            // Timeout.
            this.warn(`${item.url} timed out`);
            self.backOff();
            cb(error);
            return;
          }

          if ((error === null || error === void 0 ? void 0 : (_error$response = error.response) === null || _error$response === void 0 ? void 0 : _error$response.status) >= 500) {
            // there's an issue with the server, so let's backoff and retry
            console.warn(`${item.url} failed: 500* error`);
            self.backOff();
            cb(error);
            return;
          } // let the application deal with any other error codes
          // item._resolver(error)


          cb(error);
          self.backOff();
          return;
        }

        this.warn(`${item.url} failed: ${error.message}`);
        cb(error);
      }
    };

    this.backOff = () => {
      if (!this._currentBackoffTimer) {
        this._okToRequest = false;
        this._lastBackoff += 1000;
        const seconds = this._lastBackoff / 1000;
        console.log(`Backing off more requests for ${seconds} seconds.`);
        this._currentBackoffTimer = setTimeout(() => {
          console.log('resuming');
          this._okToRequest = true;
          this._currentBackoffTimer = false;
        }, this._lastBackoff);
      }
    };

    this.okToRequest = cb => {
      cb(null, this._okToRequest);
    };

    this._okToRequest = true;
    this._lastBackoff = 0;
    this._currentBackoffTimer = false;
    this._settings = {
      concurrent,
      maxTimeout,
      maxRetries,
      retryDelay
    };
    this._queue = new Queue(this.handleQueueItem, { ...this._settings,
      preconditionRetryTimeout: 1000,
      precondition: this.okToRequest
    });

    if (typeof cacheGet === "function" && typeof cacheSet === "function") {
      this._instance = new EtagRequest({
        cacheGet,
        cacheSet
      });
    } else {
      this._instance = axios.create();
    }

    this._reporter = reporter;
  }

  warn(msg) {
    var _this$_reporter;

    if (typeof (this === null || this === void 0 ? void 0 : (_this$_reporter = this._reporter) === null || _this$_reporter === void 0 ? void 0 : _this$_reporter.warn) !== "undefined") {
      this._reporter.warn(msg);
    } else {
      console.warn(msg);
    }
  }

  log(msg) {
    var _this$_reporter2;

    if (typeof (this === null || this === void 0 ? void 0 : (_this$_reporter2 = this._reporter) === null || _this$_reporter2 === void 0 ? void 0 : _this$_reporter2.verbose) !== "undefined") {
      this._reporter.verbose(msg);
    }
  }

  stats() {
    return this._queue.getStats();
  }

}

module.exports = RequestQueue;