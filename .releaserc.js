module.exports = {
  branches: [
    "production",
    { name: "beta", prerelease: true },
    { name: "alpha", prerelease: true },
  ],
  plugins: [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/npm",
    [
      "@semantic-release/gitlab",
      {
        assets: [
          "*.{js,css,scss,svg,png,gql}",
          {
            path: "README.md",
            label: "Documentation",
          },
          {
            path: "LICENSE",
            label: "License",
          },
        ],
      },
    ],
    [
      "@semantic-release/git",
      {
        assets: ["package.json"],
        message:
          "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",
      },
    ],
  ],
};
